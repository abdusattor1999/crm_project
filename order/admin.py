from django.contrib import admin
from .models import TovarlarKirim, Order, OrderedItem

@admin.register(TovarlarKirim)
class Kirim(admin.ModelAdmin):
    list_display = ('name','olingan_narxi','sotish_narxi','created_by','created')
    list_filter = ('category','created_by','created')
    search_fields = ('name','created','olingan_narxi','sotish_narxi')
    ordering = ('created','name','category')


# @admin.register(OrderedItem)
class OrderItemInline(admin.TabularInline):
        model = OrderedItem
        fields = ['order','product','price','quantity']



@admin.register(Order)
class Sotilgan(admin.ModelAdmin):
    inlines = [OrderItemInline]
    list_display = ('first_name','olingan_narx','sotilgan_narx','paid','created')
    list_filter = ('olingan_narx','paid','created')
    ordering = ('paid','created','updated')