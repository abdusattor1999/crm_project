from django.urls import path
from .views import  order_create

app_name = 'order'

urlpatterns = [
    # path("<int:id>/<slug:slug>",order_detail, name="detail"),
    # # path('add/',order_add, name='add'),
    path("",order_create, name="order_create"),
    # path('<int:id>/<slug:slug>', order_remove, name='delete')
]