from django.db import models
from django.shortcuts import render
from django.contrib.auth.models import User
from product.models import Category , Product 
from django.utils import timezone
from decimal import Decimal
from django.urls import reverse


class TovarlarKirim(models.Model):
    name = models.CharField(max_length=40, verbose_name='Tovar nomi')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Kategoriyasi')
    olingan_narxi = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Olingan narxi')
    sotish_narxi = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Sotilish narxi')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Kim tomonidan olingan')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Olingan kun')
    # writen = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name



class Order(models.Model):
    first_name = models.CharField(max_length=50, verbose_name='Ismi')
    last_name = models.CharField(max_length=50, verbose_name='Familiyasi')
    olingan_narx = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Olingan narxi',null=True,blank=True)
    sotilgan_narx = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Sotilgan narxi',null=True,blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    paid = models.BooleanField(default=False, verbose_name="To'langanligi")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Sotilgan sana')
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan sana")

    def __str__(self):  
        return f'{self.id}'

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())
    
    def get_absolute_url(self):
        return reverse('account:order_detail', args=[self.pk])



class OrderedItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='order_items')
    price = models.DecimalField(max_digits=12, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    quantity = models.PositiveIntegerField(default=1)
    
    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity

