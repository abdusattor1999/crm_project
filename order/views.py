from django.shortcuts import render
from .models import OrderedItem
from .forms import OrderCreateForm
from cart.cart import Cart
from product.models import Product

def order_create(request):
	cart = Cart(request)
	if request.method == 'POST':
		form = OrderCreateForm(request.POST)
		if form.is_valid():
			order = form.save(commit=False)
			order.created_by = request.user
			order.save()
			for item in cart:
				price = str(item['price'])
				quantity = str(item['quantity'])
				OrderedItem.objects.create( order=order, product=item['product'], price=price, quantity=quantity) 

				# product = Product.objects.filter(id=item['product'].pk).first()
				# product.quantity = product.quantity - item['quantity']
				# product.save()
			cart.clear()
			contex = {'cart':cart, 'form':form, 'order':order }
			return render(request, 'order/created.html', contex)
		else:
			print(form)
	else:
		form = OrderCreateForm()
	return render(request, 'order/create.html', {'form':form, 'cart':cart})

		