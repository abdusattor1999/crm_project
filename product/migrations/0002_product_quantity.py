# Generated by Django 3.1 on 2022-07-07 09:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='quantity',
            field=models.PositiveIntegerField(default=100, verbose_name='mahsulot soni'),
            preserve_default=False,
        ),
    ]
