from django.contrib import admin
from .models import Product, Category

@admin.register(Category)
class CategAdmin(admin.ModelAdmin):
    list_display = ('name','created_by','created','updated')
    list_filter = ('created_by','created','updated')
    search_fields = ('name', 'created_by')
    ordering = ('name','created','updated')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','sotish_narxi','available','quantity','created','updated')
    list_filter = ('category','olingan_narxi','sotish_narxi','available','created_by','created','updated')
    list_editable = ('sotish_narxi','available')
    search_fields = ('name','desctibtion','sotish_narxi','created_by')
    ordering = ('name','category','available','created','updated')