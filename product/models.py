from django.db import models
from account.models import Hodimlar, Haridorlar
from autoslug import AutoSlugField
from django.urls import reverse

class Category(models.Model):
    name = models.CharField(max_length=30, verbose_name='Kategoriyalar')
    slug = AutoSlugField(populate_from='name', unique=True)
    created_by = models.ForeignKey(Hodimlar, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("product:product_list_by_category", args=[self.slug])
    

class Product(models.Model):
    name = models.CharField(max_length=30, verbose_name='Mahsulot nomi')
    slug = AutoSlugField(populate_from='name',unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Kategoriyasi')
    image = models.ImageField(upload_to='Product_images/%Y/%m/%d', verbose_name='Mahsulot rasmi')
    describtion = models.TextField(max_length=1000, verbose_name='Mahsulot haqida')
    olingan_narxi = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Olingan narxi')
    sotish_narxi = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Sotilish narxi')
    available = models.BooleanField(default=True, verbose_name='Sotuvda mavjudligi')
    quantity = models.PositiveIntegerField(verbose_name='mahsulot soni')
    created_by = models.ForeignKey(Hodimlar,on_delete=models.CASCADE, verbose_name="Kim tomonidan qo'shildi")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("product:detail", args=[self.id, self.slug])
    


