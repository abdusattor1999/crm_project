from django.urls import path, include
# from django.contrib.auth import views as auth_views
from . import views

app_name = 'account'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('', views.user_login, name='login'),
    path("edit/", views.edit, name="edit"),
    # path("/", include('django.contrib.auth.urls')),
    path('dashboard/', views.dashboard, name='dashboard'),
    path("admin-product-list/",views.ad_pr_list, name="adprlist"),
    path("admin-order-list/",views.ad_or_list, name="adorlist"),
    path("admin-hodim-list/",views.ad_ho_list, name="aduserlist"),
    path("admin-haridor-list/",views.ad_ha_list, name="adhalist"),
    path("<int:pk>/",views.order_detail, name="order_detail"),
    # path('password_change/', auth_views.PasswordChangeView.as_view(),name='password_change'),
    # path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    # path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    # path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    # path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),




    path('panel/',views.admin_panel, name='admin')
]