from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm
from django.contrib.auth.decorators import login_required
from .models import Hodimlar, Haridorlar
from product.models import Product
from order.models import Order, OrderedItem

@login_required
def dashboard(request):
    return render(request, 'registration/dashboard.html',{'section':dashboard} )

@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.myUser, data=request.POST, files=request.FILES)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.myUser)
    return render(request, 'registration/edit.html', locals())



def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request, username=cd['username'], password=cd['password'])

        if user is not None:
            if user.is_active:
                login(request, user)
                if user.is_superuser:
                    return redirect('account:admin')
                else:
                    return redirect('product:product_list')
            else:
                return HttpResponse('Hisob Faol holatda emas')
        else:
            return HttpResponse("Hisobga kirishda xotilik")

    else:
        form = LoginForm()
    return render(request, 'registration/login.html', {'form':form})

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            Hodimlar.objects.create(user=new_user)

            return render(request, 'registration/register_done.html', {'new_user':new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'registration/signup.html',{'user_form':user_form})

    

def admin_panel(request): 
    return render(request,'registration/admin_panel.html')

def ad_pr_list(request):
    products = Product.objects.all()
    return render(request, 'panel/pr_list.html', {'products':products})


def ad_ho_list(request):
    hodimlar = Hodimlar.objects.all()
    return render(request, 'panel/hodim_list.html', {'hodimlar':hodimlar})

def ad_or_list(request):
    orders = Order.objects.all()
    context = {
        'orders':orders
    }
    return render(request, 'panel/order_list.html', context)

# def order_items(request, item):
#     order_items = OrderedItem.objects.all()
#     return render(request, 'panel/order_list.html')


def ad_ha_list(request):
    haridorlar = Haridorlar.objects.all()
    return render(request, 'panel/haridor_list.html', {'haridorlar':haridorlar})

def order_detail(request, pk):
    order_item = OrderedItem.objects.filter(order__pk=pk).all()
    order = Order.objects.filter(pk=pk).first()
    return render(request, 'order/order_detail.html', locals())








