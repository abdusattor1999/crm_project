from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField

class Lavozim(models.Model):
    name = models.CharField(max_length=50, verbose_name='Lavozimi')
    description = models.TextField(max_length='1000')
    created_by = models.ForeignKey(User,on_delete=models.CASCADE, related_name='tomonidan')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan sana")
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Hodimlar(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='myUser')
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True, null=True)
    tel = PhoneNumberField(verbose_name='Telefon Raqami', blank=True, null=True)
    lavozim = models.ForeignKey(Lavozim, on_delete=models.CASCADE, verbose_name='Lavozimi', blank=True, null=True)
    active = models.BooleanField(default=True , verbose_name='Aktivligi')
    created_by = models.ForeignKey(User,on_delete=models.CASCADE, verbose_name="Kim tomonidan qo'shilgan", blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartitish qilingan vaqti")

    def __str__(self):
        return f'{self.user.username}'

class Haridorlar(models.Model):
    name = models.CharField(max_length=40, verbose_name='Ism Familiya')
    tel = PhoneNumberField(verbose_name='Telefon Raqami')
    email = models.EmailField(verbose_name='Email manzili', null=True, blank=True)
    manzil = models.CharField(max_length=100, verbose_name='Manzil')
    created_by = models.ForeignKey(User,on_delete=models.CASCADE, verbose_name="Kim tomonidan qo'shildi")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan sana")
    updated = models.DateTimeField(auto_now=True, verbose_name="Yangilangan sana")

    def __str__(self):
        return self.name