from django.contrib import admin
from .models import Lavozim, Hodimlar, Haridorlar


@admin.register(Lavozim)
class LavozimAdmin(admin.ModelAdmin):
    list_display = ('name','description','created_by','created','updated')
    list_filter = ('created_by','created')
    search_fields = ('name', 'description','created_by')
    ordering = ('name', 'created','updated')


@admin.register(Hodimlar)
class HodimAdmin(admin.ModelAdmin):
    list_display = ('user','tel','lavozim','active','created_by')
    list_filter = ('lavozim','active','created_by')
    search_fields = ('user','tel','lavozim','email')
    ordering = ('lavozim','active','updated')


@admin.register(Haridorlar)
class HaridorAdmin(admin.ModelAdmin):
    list_display = ('name','tel','manzil','created_by','created')
    list_filter = ('manzil','created_by','created','name')
    search_fields = ('name', 'tel','manzil','email')
    ordering = ('name','manzil','created')