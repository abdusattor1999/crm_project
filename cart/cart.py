from decimal import Decimal
from django.conf import settings
from product.models import Product

# Bu xarid qilish aravachasiini boshqarish uchun javobgar bo'lgan Cart sinfidir
class Cart(object):

	def __init__(self, request):
		self.session = request.session
		# savat ma'lumotlarini olishga harakat qilamiz
		cart = self.session.get(settings.CART_SESSION_ID) 
		if not cart:
			# Agar biz savat ob'ektini olmasak, uni sessiyada bo'sh lug'at sifatida yaratamiz
			# Ushbu lug'atda kalitlar mahsulot id si bo'ladi va qiymatlar miqdor va narx bo'ladi
			# Savatni lug'at sifatida saqlash uning elementlariga kirishni osonlashtiradi
			cart = self.session[settings.CART_SESSION_ID] = {} 
		self.cart = cart

	def __iter__(self):
		""" Savatga qo'yilgan mahsulotlar ro'yxatini ko'rsatish uchun 
		biz Mahsulot ob'ektlari bo'ylab aylanish imkoniyatiga ega bo'lish uchun """
			
		# Savatga tegishli Mahsulot ob'ektlarini oling 
		product_ids = self.cart.keys()
		# Mahsulot modeli ob'ektlarini olamiz va ularni savatga o'tkazamiz
		products = Product.objects.filter(id__in=product_ids)
		cart = self.cart.copy()
		for product in products:
			cart[str(product.id)]['product'] = product
		for item in cart.values():
			item['price'] = Decimal(item['price'])
			item['total_price'] = item['price'] * item['quantity']
			yield item # funksiani to'xtatmasdan qaytaradi
			
		# Ushbu usulda biz savat ob'ektining nusxasini yaratamiz, 
		# unda saqlangan narsalarni olamiz. Har bir mahsulot uchun biz narxni 
		# satrdan raqamga qat'iy aniqlik bilan aylantiramiz, narx va miqdorni hisobga
		# olgan holda umumiy xarajat, jami_narxni hisoblaymiz. 
		# Endi bizda sessiyada saqlangan har bir element haqida barcha ma'lumotlar mavjud.

	def __len__(self):
		"""Savatdagi narsalarning umumiy sonini qaytarish uchun."""
		return sum(item['quantity'] for item in self.cart.values())

	def add(self, product, quantity=1, update_quantity=False):
		# Savatga mahsulot qo'shish yoki uning miqdorini yangilash uchun
		product_id = str(product.id)
		if product_id not in self.cart:
			self.cart[product_id] = {'quantity': 0, 'price': str(product.sotish_narxi)}
		if update_quantity:
			self.cart[product_id]['quantity'] = quantity
		else:
			self.cart[product_id]['quantity'] += quantity
		self.save()

	def save(self):
		# Seansni o'zgartirilgan deb belgilang
		self.session.modified = True

	def remove(self, product):
		# Buyumni savatdan olib tashlash
		product_id = str(product.id)
		if product_id in self.cart:
			del self.cart[product_id]
			self.save()

	def clear(self):
		# Savatni tozalash uchun
		del self.session[settings.CART_SESSION_ID]
		self.save()

	def get_total_price(self):
		""" Savatdagi narsalarning umumiy narxini qaytarish uchun """
		return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())
